﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CitasICAVE.Model
{
    public class MergePdfSharp
    {
        public MergePdfSharp()
        {
        }
        public void Merge(List<byte[]> _listToMerge,string _pathResult)
        {

            var fileStream = new FileStream(_pathResult, FileMode.Create);
            var document = new Document(PageSize.A4);
            var pdfCopy = new PdfCopy(document, fileStream);
            document.Open();

            foreach (var archivo in _listToMerge)
            {
                pdfCopy.AddDocument(new PdfReader(archivo));
                PdfReader.unethicalreading = true;
            }
            if (document != null)
            {
                document.Close();
                document = null;
            }
        }
    }
}
