﻿using System;

namespace CitasICAVE.Model
{
    public class GenericEventArgs<T>:EventArgs
    {
        public T Result;
        public GenericEventArgs(T _Result)
        {
            this.Result = _Result;
        }
    }
}