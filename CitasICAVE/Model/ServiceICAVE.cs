﻿using CitasICAVE.ServiceIcaveCita;
using System;
using System.Configuration;

namespace CitasICAVE.Model
{
    public class ServiceICAVE
    {
        IcaveWsClientesClient clientICAVE;
        bookingMain booking;

        public event EventHandler<GenericEventArgs<IcaveBookingResponse>> SolicitarCita_Completed;

        public ServiceICAVE()
        {
            clientICAVE = new IcaveWsClientesClient();
            var user = ConfigurationManager.AppSettings.Get("USER");
            var pass = ConfigurationManager.AppSettings.Get("PASS");

            booking = new bookingMain()
            {
                user = user,//"1674.EFMG",
                hash =pass,// "37hdfy739ZzZdfWsdw21bbs",
                timestamp = DateTime.Now.ToString("dd-MM-yyy H:mm")
            };
        }

        public async void SolicitarCita(bookingContainers[] _contenedores)
        {
            booking.bookingContainers = _contenedores;
            var result=await clientICAVE.IcaveBookingAsync(booking);
            SolicitarCita_Completed?.Invoke(this, new GenericEventArgs<IcaveBookingResponse>(result));
        }
    }
}