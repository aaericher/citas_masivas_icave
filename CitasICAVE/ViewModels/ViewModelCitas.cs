﻿using CitasICAVE.Model;
using CitasICAVE.ServiceIcaveCita;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace CitasICAVE.ViewModels
{
    public class ViewModelCitas : NotificationEnabledObject
    {
        ServiceICAVE serviceIcave;
        ExcelRead excelRead;
        public ViewModelCitas()
        {
            serviceIcave = new ServiceICAVE();
            excelRead = new ExcelRead();

            excelRead.LeerExcel_Completed += (s, a) =>
            {
                Containers = new ObservableCollection<bookingContainers>(a.Result);
                IsBusy = false;
                PermitirCitar = true;
            };
            serviceIcave.SolicitarCita_Completed += (s, a) =>
            {
                var hayError = false;
                var res = a.Result;

                var folder = $@"C:\CITAS_ICAVE\CITA_{DateTime.Now.ToString("dd-mm-yyyy_HH-mm-ss")}\";
                var RUTA_LOG = folder + "error.txt";

                if (!Directory.Exists(folder))
                    Directory.CreateDirectory(folder);

                foreach (var item in res.@return)
                {
                    if (item.status == "OK")
                    {
                        MergePdfSharp merge = new MergePdfSharp();
                        var listToMerge = new List<byte[]>();

                        var urlFileAfterMerge = $"{folder}{item.container}.pdf";
                        
                        listToMerge.Add(item.document);
                        listToMerge.Add(item.art23);

                        merge.Merge(listToMerge, urlFileAfterMerge);
                    }
                    else
                    {
                        hayError = true;
                        var message = $"{item.container}: {item.status}";

                        if (!File.Exists(RUTA_LOG))
                            File.Create(RUTA_LOG).Close();

                        using (StreamWriter w = File.AppendText(RUTA_LOG))
                            w.WriteLine(message);
                    }
                }
                Process.Start("explorer.exe", folder);
                if (hayError)
                    Process.Start(RUTA_LOG);
                IsBusy = false;
            };
        }
        private bool isBusy;

        public bool IsBusy
        {
            get => isBusy;
            set { isBusy = value; OnPropertyChanged(); }
        }
        private bool permitirCitar;

        public bool PermitirCitar
        {
            get => permitirCitar;
            set { permitirCitar = value; OnPropertyChanged(); }
        }

        private string urlFile;

        public string UrlFile
        {
            get { return urlFile; }
            set { urlFile = value;OnPropertyChanged(); }
        }

        private ObservableCollection<bookingContainers> containers;

        public ObservableCollection<bookingContainers> Containers
        {
            get
            {
                if (containers == null) containers = new ObservableCollection<bookingContainers>();
                return containers;
            }
            set
            {
                containers = value;
                OnPropertyChanged();
            }
        }
        private ActionCommand<string> openFileCommand;

        public ActionCommand<string> OpenFile_Command
        {
            get
            {
                if (openFileCommand == null)
                {
                    openFileCommand = new ActionCommand<string>((param) =>
                      {

                          // Create OpenFileDialog
                          var dlg = new OpenFileDialog()
                          {
                              DefaultExt = ".xls",
                              Filter = "Text documents (.xls)|*.xlsx;*.xls"
                          };

                          // Display OpenFileDialog by calling ShowDialog method
                          bool? result = dlg.ShowDialog();

                          // Get the selected file name and display in a TextBox
                          if (result == true)
                          {
                              IsBusy = true;
                              // Open document
                              string filename = dlg.FileName;

                              UrlFile = filename;
                              excelRead.LeerExcel(filename);
                          }
                      });
                }
                return openFileCommand;
            }
            set { openFileCommand = value; OnPropertyChanged(); }
        }

        private ActionCommand<string> programarCitas_Command;
        public ActionCommand<string> ProgramarCitas_Command
        {
            get
            {
                if (programarCitas_Command == null)
                    programarCitas_Command = new ActionCommand<string>((param) =>
                    {
                        IsBusy = true;
                        serviceIcave.SolicitarCita(containers.ToList().ToArray());
                    });
                return programarCitas_Command;
            }
            set { programarCitas_Command = value; }
        }
    }
}