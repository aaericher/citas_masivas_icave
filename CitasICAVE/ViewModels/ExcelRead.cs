﻿using CitasICAVE.Model;
using CitasICAVE.ServiceIcaveCita;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;

namespace CitasICAVE.ViewModels
{
    public class ExcelRead
    {
        public event EventHandler<GenericEventArgs<List<bookingContainers>>> LeerExcel_Completed;

        Application xlApp;
        Workbook xlWorkbook;
        _Worksheet xlWorksheet;
        Range xlRange;

        public void LeerExcel(string filename)
        {
            var readExcelThread = new Thread(() =>
             {
                 var containers = new List<bookingContainers>();

                 //Create COM Objects. Create a COM object for everything that is referenced
                 xlApp = new Application();
                 xlWorkbook = xlApp.Workbooks.Open(filename);
                 xlWorksheet = xlWorkbook.Sheets[1];
                 xlRange = xlWorksheet.UsedRange;
                 //iterate over the rows and columns and print to the console as it appears in the file
                 //excel is not zero based!!

                 int rowCount = xlRange.Rows.Count;
                 int colCount = xlRange.Columns.Count;
                 var content = string.Empty;
                 for (int i = 2; i <= rowCount; i++)
                 {
                     if (xlRange?.Cells[i, 1]?.Value?.ToString() != null  && xlRange?.Cells[i, 2]?.Value?.ToString() != null&& xlRange?.Cells[i, 3]?.Value?.ToString() != null)
                     {
                         bookingContainers container = new bookingContainers();

                         container.broker =  "1674";
                         container.exporter = "EII 061116R76";
                         container.cargo_type = "DC";
                         container.commodities = "CERVEZA";
                         container.reg_expo = "H1";

                         container.container = xlRange?.Cells[i, 1]?.Value?.ToString();
                         container.seal1 = xlRange?.Cells[i, 2]?.Value?.ToString();
                         container.booking = xlRange?.Cells[i, 3]?.Value?.ToString();
                         container.iso = xlRange?.Cells[i, 4]?.Value?.ToString();
                         container.final_destiny = xlRange?.Cells[i, 5]?.Value?.ToString();
                         container.line = xlRange?.Cells[i, 6]?.Value?.ToString();
                         container.vessel_reference = xlRange?.Cells[i, 7]?.Value?.ToString();
                         container.gross_weight = xlRange?.Cells[i, 8]?.Value?.ToString();
                         container.tare_weight = xlRange?.Cells[i, 9]?.Value?.ToString();

                         string cellPort = Convert.ToString(xlRange?.Cells[i, 10]?.Value);
                         var cellPortArray = cellPort.Trim().Split(new char[] { '-'});
                         container.discharge_port = cellPortArray.Last().Trim();

                         containers.Add(container);
                     }
                 }
                 //cleanup
                 GC.Collect();
                 GC.WaitForPendingFinalizers();

                 //rule of thumb for releasing com objects:
                 //  never use two dots, all COM objects must be referenced and released individually
                 //  ex: [somthing].[something].[something] is bad

                 //release com objects to fully kill excel process from running in the background
                 Marshal.ReleaseComObject(xlRange);
                 Marshal.ReleaseComObject(xlWorksheet);

                 //close and release
                 xlWorkbook.Close();
                 Marshal.ReleaseComObject(xlWorkbook);

                 //quit and release
                 xlApp.Quit();
                 Marshal.ReleaseComObject(xlApp);

                 App.Current.Dispatcher.Invoke(() =>
                 LeerExcel_Completed?.Invoke(this, new GenericEventArgs<List<bookingContainers>>(containers)));
             });
            readExcelThread.IsBackground = true;
            readExcelThread.Start();
        }
    }
}