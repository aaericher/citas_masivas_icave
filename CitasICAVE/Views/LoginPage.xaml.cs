﻿using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Xml.Linq;

namespace CitasICAVE.Views
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        public LoginPage()
        {
            Loaded += LoginPage_Loaded;
        }

        void LoginPage_Loaded(object sender, RoutedEventArgs e)
        {
            ImageSourceConverter isource = new ImageSourceConverter();
            ImageBrush brush = new ImageBrush()
            {
                Stretch = Stretch.UniformToFill,
                ImageSource = (ImageSource)isource.ConvertFromString(GetUrlFromBing())
            };
            LayoutRoot.Background = brush;
        }

        private string GetUrlFromBing()
        {
            var request = HttpWebRequest.Create(@"http://www.bing.com/HPImageArchive.aspx?format=xml&idx=0&n=1&mkt=en-US");
            request.ContentType = "application/xml";
            request.Method = "GET";

            string url = string.Empty;
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                if (response.StatusCode == HttpStatusCode.OK)
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        var content = reader.ReadToEnd();
                        if (!string.IsNullOrWhiteSpace(content))
                        {
                            XDocument doc = XDocument.Parse(content);
                            url = "http://www.bing.com" + doc.Element("images").Element("image").Element("url").Value;
                        }
                    }
            return url;
        }
    }
}
